#!/bin/bash

export NAME=$(basename $PWD)
export TAG=latest

docker build -t $NAME:$TAG .

docker run -it --rm \
    $* \
    -p 80:80 \
    --network iaas \
    --name $NAME \
    --hostname $NAME \
    $NAME:$TAG

#docker image prune -a -f