# Status

[gitlab-pipeline-image]: https://gitlab.com/mydcs/mail/webmail/badges/main/pipeline.svg
[gitlab-pipeline-link]: https://gitlab.com/mydcs/mail/webmail/-/commits/main
[gitlab-release-image]: https://gitlab.com/mydcs/mail/webmail/-/badges/release.svg
[gitlab-release-link]: https://gitlab.com/mydcs/mail/webmail/-/releases
[gitlab-stars-image]: https://img.shields.io/gitlab/stars/mydcs/mail/webmail?gitlab_url=https%3A%2F%2Fgitlab.com
[gitlab-stars-link]: https://hub.docker.com/r/mydcs/mail/webmail

[docker-pull-image]: https://img.shields.io/docker/pulls/mydcs/webmail.svg
[docker-pull-link]: https://hub.docker.com/r/mydcs/webmail
[docker-release-image]: https://img.shields.io/docker/v/mydcs/webmail?sort=semver
[docker-release-link]: https://hub.docker.com/r/mydcs/webmail
[docker-stars-image]: https://img.shields.io/docker/stars/mydcs/webmail.svg
[docker-stars-link]: https://hub.docker.com/r/mydcs/webmail
[docker-size-image]: https://img.shields.io/docker/image-size/mydcs/webmail/latest.svg
[docker-size-link]: https://hub.docker.com/r/mydcs/webmail


[![gitlab-pipeline-image]][gitlab-pipeline-link] 
[![gitlab-release-image]][gitlab-release-link]
[![gitlab-stars-image]][gitlab-stars-link]


[![docker-pull-image]][docker-pull-link] 
[![docker-release-image]][docker-release-link]
[![docker-stars-image]][docker-stars-link]
[![docker-size-image]][docker-size-link]

# How To

## Vorbereitung

1. Arbeitsverzeichnis anlegen
2. In das Arbeitsverzeichnis wechseln
3. Unterordner config anlegen
4. Container starten, entweder ueber "docker run" oder "docker compose"

## Aufruf

### CLI

```
docker run -it --rm -e DOMAIN=example -e TLD=com -v $(pwd)/config:/srv -v /etc/letsencrypt:/etc/letsencrypt mydcs/webmail:latest
```

### Compose

```dockerfile
version: "3.8"

services:
  webmail:
    container_name: webmail
    hostname: webmail
    restart: always
    image: mydcs/webmail:latest
    environment:
      - DOMAIN=${DOMAIN:-example}
      - TLD=${TLD:-com}
    networks:
      - dcs
    ports:
      - 8080:8080
    volumes:
      - $PWD/config:/srv
      - /etc/letsencrypt:/etc/letsencrypt

networks:
  dcs:
    name: dcs
```
