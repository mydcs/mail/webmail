# This is a GitLab CI configuration to build the project as a docker image
# The file is generic enough to be dropped in a project containing a working Dockerfile
# Author: Florent CHAUVEAU <florent.chauveau@gmail.com>
# Mentioned here: https://blog.callr.tech/building-docker-images-with-gitlab-ci-best-practices/

# do not use "latest" here, if you want this to work in the future
image: docker:20

#services:
#  - docker:dind

stages:
  - build
  - push
  - release
  - publish-readme

variables:
  DOCKER_USERNAME: $DOCKER_USERNAME
  DOCKER_PASSWORD: $DOCKER_PASSWORD

# Use this if your GitLab runner does not use socket binding
#services:
#  - docker:dind

Build:
  stage: build
  script:
    - echo "###############################################################################"
    - echo "build $CI_PROJECT_NAME:$CI_COMMIT_REF_NAME"
    - echo "###############################################################################"
    - echo "DOCKER_USERNAME $DOCKER_USERNAME"
    - echo DOCKER_USERNAME $DOCKER_USERNAME	
    - echo -n "$DOCKER_PASSWORD" | docker login -u "$DOCKER_USERNAME" --password-stdin 
    - docker buildx create --use
    - >
      docker buildx build --push
      --platform linux/arm/v7,linux/amd64
      --label "org.opencontainers.image.title=$CI_PROJECT_TITLE"
      --label "org.opencontainers.image.url=$CI_PROJECT_URL"
      --label "org.opencontainers.image.created=$CI_JOB_STARTED_AT"
      --label "org.opencontainers.image.revision=$CI_COMMIT_SHA"
      --label "org.opencontainers.image.licenses=$CI_COMMIT_SHA"
      --label "org.opencontainers.image.version=$CI_COMMIT_REF_NAME"
      --tag $CI_PROJECT_ROOT_NAMESPACE/$CI_PROJECT_NAME:$CI_COMMIT_SHA
      .

# Here, the goal is to tag the "main" branch as "latest"
Push latest:
  variables:
    # We are just playing with Docker here. 
    # We do not need GitLab to clone the source code.
    GIT_STRATEGY: none
  stage: push
  only:
    # Only "main" should be tagged "latest"
    - main
  script:
    - echo "###############################################################################"
    - echo "push image $CI_REGISTRY_IMAGE:latest"
    - echo "###############################################################################"
    - echo -n "$DOCKER_PASSWORD" | docker login -u "$DOCKER_USERNAME" --password-stdin 
    # Because we have no guarantee that this job will be picked up by the same runner 
    # that built the image in the previous step, we pull it again locally
    - docker pull $CI_PROJECT_ROOT_NAMESPACE/$CI_PROJECT_NAME:$CI_COMMIT_SHA
    # Then we tag it "latest"
    - docker tag $CI_PROJECT_ROOT_NAMESPACE/$CI_PROJECT_NAME:$CI_COMMIT_SHA $CI_PROJECT_ROOT_NAMESPACE/$CI_PROJECT_NAME:latest
    # And we push it.
    - docker push $CI_PROJECT_ROOT_NAMESPACE/$CI_PROJECT_NAME:latest

# Finally, the goal here is to Docker tag any Git tag
# GitLab will start a new pipeline everytime a Git tag is created, which is pretty awesome
Push tag:
  variables:
    # Again, we do not need the source code here. Just playing with Docker.
    GIT_STRATEGY: none
  stage: push
  only:
    # We want this job to be run on tags only.
    - tags
  script:
    - echo "###############################################################################"
    - echo "push image $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME"
    - echo "###############################################################################"
    - echo -n "$DOCKER_PASSWORD" | docker login -u "$DOCKER_USERNAME" --password-stdin 
    # Because we have no guarantee that this job will be picked up by the same runner 
    # that built the image in the previous step, we pull it again locally
    - docker pull $CI_PROJECT_ROOT_NAMESPACE/$CI_PROJECT_NAME:$CI_COMMIT_SHA
    # Then we tag it "latest"
    - docker tag $CI_PROJECT_ROOT_NAMESPACE/$CI_PROJECT_NAME:$CI_COMMIT_SHA $CI_PROJECT_ROOT_NAMESPACE/$CI_PROJECT_NAME:$CI_COMMIT_REF_NAME
    # And we push it.
    - docker push $CI_PROJECT_ROOT_NAMESPACE/$CI_PROJECT_NAME:$CI_COMMIT_REF_NAME

Release:
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:v0.15.0
  rules:
    - if: $CI_COMMIT_TAG                 # Run this job when a tag is created
  script:
    - echo "running release_job"
  release:                               # See https://docs.gitlab.com/ee/ci/yaml/#release for available properties
    tag_name: '$CI_COMMIT_TAG'
    description: '$CI_COMMIT_TAG'

publish-readme:
  stage: publish-readme
  only:
    - tags
  script:
    - echo -n "$DOCKER_PASSWORD" | docker login -u "$DOCKER_USERNAME" --password-stdin 
    - echo "Install curl and jq"
    - apk --no-cache add curl jq
    - echo "Create JSON String for login"
    - >
      export LOGIN=$(echo '{}' | jq 
      --arg username "$DOCKER_USERNAME"
      --arg password "$DOCKER_PASSWORD"
      '. | .username= $username | .password= $password'
      )
    - >
      curl -X POST -H "Content-Type: application/json" -o /tmp/token.txt -d "$LOGIN" https://hub.docker.com/v2/users/login
    - export TOKEN=$(cat /tmp/token.txt | jq -r '.token' )
    - >
      export UPDATE=$(echo '{}' | jq
      --arg token "$TOKEN"
      --arg repository "$CI_PROJECT_ROOT_NAMESPACE/$CI_PROJECT_NAME"
      --arg description "$CI_PROJECT_URL $CI_PROJECT_DESCRIPTION"
      --arg full_description "$(cat README.md)"
      ' . | .token=$token | .repository=$repository | .description=$description | .full_description=$full_description'
      )
    - export URL="https://hub.docker.com/v2/repositories/$CI_PROJECT_ROOT_NAMESPACE/$CI_PROJECT_NAME"
    - >
      curl -X PATCH -H "Content-Type: application/json" -H "Authorization: JWT $TOKEN" -d "$UPDATE" $URL
